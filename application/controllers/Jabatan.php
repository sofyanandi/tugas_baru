<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jabatan extends CI_Controller {

	public function __construct()
	{
		parent:: __construct();
		$this->load->model("jabatan_model");
		
		//load validasi
		$this->load->library('form_validation');
		
		//cek sesi login
		$user_login = $this->session->userdata();
		if (count($user_login)<= 1) {
			redirect("auth/index", "refresh");
			}
		
	}
	public function index()
	{
		$this->listJabatan();
	}
	public function listJabatan()
	{
		//proses pencarian data
		if (isset($_POST['tombol_cari'])) {
			$data['kata_pencarian'] = $this->input->post('cari_data');
			$this->session->set_userdata('session_pencarian_jabatan', $data['kata_pencarian']);
		} else {
			$data['kata_pencarian'] = $this->session->userdata('session_pencarian_jabatan');
		}
		
		$data['data_jabatan']	= $this->jabatan_model->tombolpagination($data['kata_pencarian']);
		
		//$data['data_jabatan'] 	= $this->jabatan_model->tampilDataJabatan();
		$data['content']		= 'forms/list_jabatan';
		$this->load->view('home', $data);
	}
	
	public function input_jabatan()
	{
		$data['data_jabatan'] = $this->jabatan_model->tampilDataJabatan();
		$data['content']		= 'forms/input_jabatan';
			
			/*if (!empty($_REQUEST)) {
				$m_jabatan = $this->jabatan_model;
				$m_jabatan->save();
				redirect("jabatan/index", "refresh");	
			}*/
			
		$validation = $this->form_validation;
		$validation->set_rules($this->jabatan_model->rules());
		
		if ($validation->run()) {
			$this->jabatan_model->save();
			$this->session->set_flashdata('info', '<div style="color: white">Simpan Data Berhasil !</div>');
			redirect("jabatan/index", "refresh");
			}
		
		//$data['content']		= 'forms/input_barang';
		
		$this->load->view('home', $data);
	}
	public function detailJabatan($kode_jabatan)
	{
		$data['detail_jabatan'] = $this->jabatan_model->detail($kode_jabatan);
		$data['content']		= 'forms/detail_jabatan';
		$this->load->view('home', $data);	
	}
	
	public function editJabatan($kode_jabatan)
	{	
		$data['detail_jabatan']	= $this->jabatan_model->detail($kode_jabatan);
		$data['content']		= 'forms/edit_jabatan';
		/*if (!empty($_REQUEST)) {
				$m_jabatan = $this->jabatan_model;
				$m_jabatan->update($kode_jabatan);
				redirect("jabatan/index", "refresh");	
			}*/
			
		$validation = $this->form_validation;
		$validation->set_rules($this->jabatan_model->rules());
		
		if ($validation->run()) {
			$this->jabatan_model->update($kode_jabatan);
			$this->session->set_flashdata('info', '<div style="color: white">Update Data Berhasil !</div>');
			redirect("jabatan/index", "refresh");
			}

		$this->load->view('home', $data);	
	}
	
	public function delete($kode_jabatan)
	{
		$m_jabatan = $this->jabatan_model;
		$m_jabatan->delete($kode_jabatan);
		$this->session->set_flashdata('info', '<div style="color: white">Hapus Data Berhasil !</div>');	
		redirect("jabatan/index", "refresh");	
	}
}
