<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis_barang extends CI_Controller {

	public function __construct()
	{
		parent:: __construct();
		$this->load->model("jenis_barang_model");
		
		//validasi
		$this->load->library('form_validation');
		
		//cek sesi login
		$user_login = $this->session->userdata();
		if (count($user_login)<= 1) {
			redirect("auth/index", "refresh");
			}
		
	}
	public function index()
	{
		$this->listjenisbarang();
	}
	public function listjenisbarang()
	{	
		//proses pencarian data
		if (isset($_POST['tombol_cari'])) {
			$data['kata_pencarian'] = $this->input->post('cari_data');
			$this->session->set_userdata('session_pencarian_jenis', $data['kata_pencarian']);
		} else {
			$data['kata_pencarian'] = $this->session->userdata('session_pencarian_jenis');
		}
		
		$data['data_jenis_barang']	= $this->jenis_barang_model->tombolpagination($data['kata_pencarian']);
		
		//$data['data_jenis_barang'] = $this->jenis_barang_model->tampilDataJenis_barang();
		$data['content']		= 'forms/list_jenis_barang';
		$this->load->view('home', $data);
	}
	
	public function input_jenis_barang()
	{		
			
			/*if (!empty($_REQUEST)) {
				$m_jenis_barang = $this->jenis_barang_model;
				$m_jenis_barang->save();
				redirect("jenis_barang/index", "refresh");	
			}*/
			$data['content']		= 'forms/input_jenis_barang';
			$validation = $this->form_validation;
			$validation->set_rules($this->jenis_barang_model->rules());
		
			if ($validation->run()) {
			$this->jenis_barang_model->save();
			$this->session->set_flashdata('info', '<div style="color: white">Simpan Data Berhasil !</div>');
			redirect("jenis_barang/index", "refresh");
			}
		
			$this->load->view('home', $data);
	}
	public function detailJenis_barang($kode_jenis)
	{
		$data['detail_jenis_barang'] = $this->jenis_barang_model->detail($kode_jenis);
		$data['content']			= 'forms/detail_jenis_barang';
		$this->load->view('home', $data);	
	}
	
	public function editJenis_barang($kode_jenis)
	{	
		$data['detail_jenis_barang']	= $this->jenis_barang_model->detail($kode_jenis);
		$data['content']				= 'forms/edit_jenis_barang';
		/*if (!empty($_REQUEST)) {
				$m_jenis_barang = $this->supplier_model;
				$m_jenis_barang->update($kode_jenis_barang);
				redirect("jenis_barang/index", "refresh");	
			}*/
		$validation = $this->form_validation;
		$validation->set_rules($this->jenis_barang_model->rules());
		
		if ($validation->run()) {
			$this->jenis_barang_model->update($kode_jenis_barang);
			$this->session->set_flashdata('info', '<div style="color: white">Update Data Berhasil !</div>');
			redirect("jenis_barang/index", "refresh");
			}
		
		$this->load->view('home', $data);	
	}
	
	 public function delete($kode_jenis)
	{
		$m_jenis_barang = $this->jenis_barang_model;
		$m_jenis_barang->delete($kode_jenis);
		$this->session->set_flashdata('info', '<div style="color: white">Hapus Data Berhasil !</div>');	
		redirect("jenis_barang/index", "refresh");	
	}
}
