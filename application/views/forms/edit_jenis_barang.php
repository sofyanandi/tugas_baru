<?php
	foreach($detail_jenis_barang as $data) 
	{
    	$kode_jenis = $data->kode_jenis;
		$nama_jenis		= $data->nama_jenis;
    }
?>

<body bgcolor="#999999">
<center><font color="#FFFFFF" size="+3">Edit Jenis Barang</font></center><br>
<div id="body" style="text-align: center;">
<div style="color:white"><?= validation_errors();?></div>
<form action="<?=base_url()?>jenis_barang/input_jenis_barang/<?=$kode_jenis?>" method="POST";>
<table width="40%" border="0" cellpadding="5" bgcolor="#FFFFFF" align="center">
  <tr>
    <td>Kode Jenis Barang</td>
    <td>:</td> 
    <td><input value="<?=$kode_jenis;?>"type="text" name="kode_jenis" id="kode_jenis" maxlength="10" 
    value="<?= set_value('kode_jenis');?>"></td>
  </tr>
  <tr>
    <td>Nama Jenis</td>
    <td>:</td>
    <td><input value="<?=$nama_jenis;?>"type="text" name="nama_jenis" id="nama_jenis" maxlength="50"
    value="<?= set_value('nama_jenis');?>"></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><input type="submit" name="submit" id="submit" value="Simpan">
      <input type="reset" name="reset" id="reset" value="Reset"><br/><br/>
      <a href="<?=base_url();?>jenis_barang/listjenisbarang""><input type="button" name="button" id="button" value="Kembali Ke Menu Sebelumnya"></a></td>
  </tr>
</table>
</form>
</body>