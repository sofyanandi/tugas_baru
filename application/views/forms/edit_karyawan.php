<?php
	foreach($detail_karyawan as $data) 
	{
    	$nik			= $data->nik;
		$nama_lengkap	= $data->nama_lengkap;
		$tempat_lahir	= $data->tempat_lahir;
		$tgl_lahir		= $data->tgl_lahir;
		$jenis_kelamin	= $data->jenis_kelamin;
		$alamat			= $data->alamat;
		$telp			= $data->telp;
		$kode_jabatan	= $data->kode_jabatan;
		$photo			= $data->photo;
    }
?>
<body bgcolor="#999999">
<center><font color="#FFFFFF" size="+3">Edit Karyawan</font></center><br>
<div id="body" style="text-align: center;">
<div style="color:white"><?= validation_errors();?></div>
<form action="<?=base_url()?>karyawan/editkaryawan/<?=$nik; ?>" method="POST" enctype="multipart/form-data">
<table width="50%" height="80%" border="0" cellpadding="5" bgcolor="#FFFFFF" align="center">
  <tr>
    <td>NIK</td>
    <td>:</td>
    <td><input value ="<?=$nik;?>" type="text" name="nik" id="nik" maxlength="10" readonly value="<?= set_value('nik');?>"> 
    </td>
  </tr>
  <tr>
    <td>Nama Karyawan</td>
    <td>:</td>
    <td><input value="<?=$nama_lengkap;?>" type="text" name="nama_karyawan" id="nama_karyawan" maxlength="50" 
    value="<?= set_value('nama_karyawan');?>"></td>
  </tr>
  <tr>
    <td>Tempat Lahir</td>
    <td>:</td>
    <td><input value="<?=$tempat_lahir;?>" type="text" name="tempat_lahir" id="tempat_lahir" maxlength="50" 
    value="<?= set_value('tempat_lahir');?>">
    </td>
  </tr>
  <tr>
    <td>Jenis Kelamin</td>
    <td>:</td>
     <?php
    	if($jenis_kelamin == 'P'){
			$slc_p = 'SELECTED';
			$slc_l = '';
		}elseif($jenis_kelamin == 'L'){
			$slc_l = 'SELECTED';
			$slc_p = '';
		}else{
			$slc_p = '';
			$slc_l = '';
		}
	?>
    <td><select name="jenis_kelamin" id="jenis_kelamin">
    	<option <?=$slc_p;?> value="P">Perempuan</option>
        <option <?=$slc_l;?> value="L">Laki-laki</option>
    </select>
    </td>
  </tr>
  <tr>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
   $( function(){
	  $( "#tgl_lahir" ).datepicker({dateFormat: "yy-mm-dd"});
	  });
  </script>
 		<td>Tanggal Lahir</td> 
 		<td>:</td>
 		<td><input type="text" name="tgl_lahir" id="tgl_lahir"> </td>
	</tr>
  
  <tr>
    <td>Telepon</td>
    <td>:</td>
    <td><input value="<?=$telp;?>"type="text" name="telp" id="telp" maxlength="50" value="<?= set_value('telepon');?>"></td>
  </tr>
  <tr>
    <td>Alamat</td>
    <td>:</td>
    <td><textarea name="alamat" id="alamat" cols="45" rows="5" value="<?= set_value('alamat');?>"><?=$alamat;?></textarea></td>
  </tr>
  <tr>
    <td>Jabatan</td>
    <td>:</td>
    <td><select name="kode_jabatan" id="jabatan">
    	<?php foreach($data_jabatan as $data) {
			$select_jabatan = ($data->kode_jabatan == 
			$kode_jabatan) ? 'selected' : '';
		?>
    	<option value="<?=$data->kode_jabatan;?>"<?=$select_jabatan;?>><?=$data->nama_jabatan;?></option><?php }?>
    	</select>
    </td>
  </tr>
  <tr>
  <td>Upload Foto</td>
  <td>:</td>
  <td>
  <input type="file" name="image" id="image">
  <input type="hidden" name="foto_old" id="foto_old" value="<?=$photo;?>">
  </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><input type="submit" name="submit" id="submit" value="Simpan">
      <input type="reset" name="reset" id="reset" value="Reset"><br/><br/>
      <a href="<?=base_url();?>karyawan/listkaryawan""><input type="button" name="button" id="button" value="Kembali Ke Menu Sebelumnya"></a></td>
  </tr>
</table>
</form>
</body>