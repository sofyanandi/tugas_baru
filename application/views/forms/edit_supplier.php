<?php
	foreach($detail_supplier as $data) 
	{
    	$kode_supplier	= $data->kode_supplier;
		$nama_supplier	= $data->nama_supplier;
		$alamat			= $data->alamat;
		$telp			= $data->telp;
    }
?>
<body bgcolor="#999999">
<center><font color="#FFFFFF" size="+3">Edit Supplier</font></center><br>
<div id="body" style="text-align: center;">
<div style="color:white"><?= validation_errors();?></div>
<form action="<?=base_url()?>supplier/editsupplier/<?=$kode_supplier?>" method="POST";>
<table width="50%" border="0" cellpadding="5" bgcolor="#FFFFFF" align="center">
  <tr>
    <td>Kode Supplier</td>
    <td>:</td>
    <td><input value="<?=$kode_supplier;?>"type="text" name="kode_supplier" id="kode_supplier" maxlength="10" value="<?= set_value('kode_supplier');?>"></td>
  </tr>
  <tr>
    <td>Nama Supplier</td>
    <td>:</td>
    <td><input value="<?=$nama_supplier;?>"type="text" name="nama_supplier" id="nama_supplier" maxlength="50" 
    value="<?= set_value('nama_supplier');?>"></td>
  </tr>
  <tr>
    <td>Alamat</td>
    <td>:</td>
    <td><textarea name="alamat" id="alamat" cols="45" rows="5" value="<?= set_value('alamat');?>"><?=$alamat;?></textarea></td>
  </tr>
  <tr>
    <td>Telepon</td>
    <td>:</td>
    <td><input value="<?=$telp;?>"type="text" name="telp" id="telp" maxlength="15" value="<?= set_value('telepon');?>">
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><input type="submit" name="submit" id="submit" value="Simpan">
      <input type="reset" name="reset" id="reset" value="Reset"><br/><br/>
      <a href="<?=base_url();?>supplier/listsupplier""><input type="button" name="button" id="button" value="Kembali Ke Menu Sebelumnya"></a></td>
  </tr>
</table>
</form>
</body>