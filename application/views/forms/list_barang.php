
<?php
		if ($this->session->flashdata('info') == true) {
			echo $this->session->flashdata('info');
			}
?>
	<table  align="center" width="90%" border="1" cellspacing="0" cellpadding="5" bordercolor="#FFFFFF" bgcolor="#FFFFFF">
 		<tr>
    		<th colspan="7" align="center" bgcolor="black"><font color="#FFFFFF">Data Barang</font></th>
    	</tr>
 
         <tr>
         	<form action="<?=base_url()?>barang/listBarang" method="POST">
         	<td><font><a href="input_barang">Input Barang</a></font></td>
    		<td colspan="6" align="right"><label for="Cari Nama"></label>
    		<input type="text" name="cari_data" id="cari_data" placeholder="Cari Nama">
    		<input name="tombol_cari" type="submit" value="cari data"></td>
  		</tr>
        <tr>
        	<th>No</th>
        	<th>Kode Barang</th>
            <th>Nama Barang</th>
            <th>Harga Barang</th>
            <th>Nama Jenis</th>
            <th>Aksi</th>
        </tr>
        <?php
	  	$data_posisi	=	$this->uri->segment(4);
		$no				= 	$data_posisi;
		if (count($data_barang) > 0) {
			foreach ($data_barang as $data) 
			{
	  	$no++;
	  	
		
	 	?>
    	<tr>
    		<td><?=$no;?></td>
            <td><?=$data->kode_barang;?></td>
            <td><?=$data->nama_barang;?></td>
            <td><?=$data->harga_barang;?></td>
            <td><?=$data->nama_jenis;?></td>
            <td><a href="<?= base_url(); ?>barang/detailbarang/<?= $data->kode_barang; ?>">Detail</a>
            	| 	<a href="<?= base_url(); ?>barang/editbarang/<?= $data->kode_barang; ?>">Edit</a>
                |	<a onClick="return confirm('Anda Yakin Ingin Hapus Data ?')"href="<?= base_url();?>barang/delete/<?= $data->kode_barang;?>">
       Delete</a> 
            </td>
    	</tr>
    	<tr>
    	</tr>
    	<?php } ?>
        </tr>
       <tr height="70px">
        <td align="center" colspan="7"> <b>Halaman : </b> <?= $this->pagination->create_links();?> </td>
        </tr>
      
    	<?php } else {  ?>
        <tr align="center">
        	<td colspan="7">--- Tidak ada Data ----</td>
  		</tr> 
        <?php } ?>
    </table>