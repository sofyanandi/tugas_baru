<?php
		if ($this->session->flashdata('info') == true) {
			echo $this->session->flashdata('info');
			}
?>
<table  align="center" width="90%" border="1" cellspacing="0" cellpadding="5" bordercolor="#FFFFFF" bgcolor="#FFFFFF">
	<tr>
    	<th colspan="7" align="center" bgcolor="black"><font color="#FFFFFF">Data Karyawan</font></th>
    </tr>
        <tr>
        	<td><font><a href="input_karyawan">Input Karyawan</a></font></td>
            <form action="<?=base_url()?>karyawan/listKaryawan" method="POST">
    		<td colspan="6" align="right"><label for="Cari Nama"></label>
    		<input type="text" name="cari_data" id="cari_data" placeholder="Cari Nama">
    		<input name="tombol_cari" type="submit" value="cari data">
          	</td>

  		</tr>
        <tr>
        	<th>No</th>
        	<th>NIK</th>  
            <th>Nama</th>
            <th>Tempat Lahir</th>
            <th>Telp</th>
            <th>Foto</th>
            <th>Aksi</th>
        </tr>
        <?php
		$data_posisi	=	$this->uri->segment(4);
		$no				= 	$data_posisi;
		if (count($data_karyawan) > 0) {
			foreach ($data_karyawan as $data) 
			{
	  	$no++;
	  	
			  
	 	?>
    	<tr align="center">
    		<td align=""><?=$no;?></td>
            <td><?=$data->nik;?></td>
            <td><?=$data->nama_lengkap;?></td>
            <td><?=$data->tempat_lahir;?></td>
            <td><?=$data->telp;?></td>
            <td><?php $photo = $data->photo;
			if (!empty($photo))
			{
			?> 
			<img src="<?=base_url(); ?>resources/foto_karyawan/<?=$data->photo;?>" width="80px" height="80px" />
            
            <?php
			}
			else
			{
			?>
            <img src="<?=base_url(); ?>resources/foto_karyawan/default.jpeg" width="80px" height="80px" />
            <?php
			}
			?>
			</td>
            
            <td><a href="<?= base_url(); ?>karyawan/detailkaryawan/<?= $data->nik; ?>">Detail</a> 
            | 	<a href="<?= base_url(); ?>karyawan/editkaryawan/<?= $data->nik; ?>">Edit</a> 
		|<a onClick="return confirm('Anda Yakin Ingin Hapus Data')"href="<?= base_url();?>karyawan/delete/<?= $data->nik;?>">
       Delete</a>
       		 
            </td>
    	<?php
			}
		?>
        
    	</tr>
       <tr height="70px">
        <td align="center" colspan="7"> <b>Halaman : </b> <?= $this->pagination->create_links();?> </td>
        </tr>
      
    	<?php } else {  ?>
        <tr align="center">
        	<td colspan="7">--- Tidak ada Data ----</td>
  		</tr> 
        <?php } ?>
        
</form>
</table>

