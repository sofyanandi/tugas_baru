<center><font size="+2" color="#FFFFFF"> Laporan Pembelian </font> </center>
<center><font size="+2" color="#FFFFFF"> Dari Tanggal <?=$tgl_awal;?> sd  <?=$tgl_akhir;?> </font> </center><br />

 <?php
		if ($this->session->flashdata('info') == true) {
			echo $this->session->flashdata('info');
			}
?>
    
	<table  align="center" width="80%" border="1" cellspacing="0" cellpadding="5" bordercolor="#FFFFFF" bgcolor="#FFFFFF">
        <tr>
    		<td colspan="8" ><font><a href="<?= base_url();?>pembelian/cetakPdf/<?=$tgl_awal;?>/<?=$tgl_akhir;?>">Cetak Pdf</a></font></td>
  		</tr>
        <tr>
        	<th>No</th>
        	<th>ID Pembelian</th>
            <th>No Transaksi</th>  
            <th>Tanggal</th>
            <th>Total Barang</th>
			<th>Total qty</th>
            <th>Jumlah Nominal Pembelian</th>
        
            <th>Aksi</th>
        </tr>
        <?php
	  	$no = 0;
		$total	= 0;
	  	foreach ($data_pembelian as $data) { $no++;
	  
	 	?>
    	<tr>
        	
    		<td><?=$no;?></td>
            <td><?=$data->id_pembelian_h;?></td>
            <td><?=$data->no_transaksi;?></td>
            <td><?=$data->tanggal;?></td>
            <td><?=$data->total_barang;?></td>
            <td><?=$data->total_qty;?></td>
            <td align="right">Rp. <?= number_format($data->total_pembelian)?></td>
            <td>
         	<a onClick="return confirm('Anda Yakin Ingin Hapus Data')"href="<?= base_url();?>pembelian/delete/<?= $data->id_pembelian_h;?>">
       Delete</a>
            </td>
    	</tr> 
            <?php
				$total += $data->total_pembelian;
	}
				
			?>
            
                <tr align="center">
        	<th align="right" colspan="6">Total Keseluruhan</th>
            
           <th align="right">Rp. <?= number_format($total);?></th>
    	</tr>
    </table>
    